$(document).ready(function(){
    $('.testimonial__slider').owlCarousel({
        loop:true,
        responsiveClass:true,
        items: 1,
        nav: true,
        dots:false,
        navText: ["<img src='../img/left-arrow.svg' />","<img src='../img/right-arrow.svg' />"],
    })
});