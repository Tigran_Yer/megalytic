// Gulp
const gulp = require('gulp');

// Templates
const ejs = require('gulp-ejs');

// Stylesheet
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const autoprefixer = require('gulp-autoprefixer');
const autoprefixerOptions = {
    browsers: ['last 2 versions', '> 5%', 'Firefox ESR'],
    cascade: false
};

// Rename Files
const rename = require("gulp-rename");

// Notify On Error
const notify = require('gulp-notify');

// Compress JS
const uglify = require('gulp-uglify');

// Rewrite images path
const rewriteImagePath = require('gulp-rewrite-image-path');

// Delete file/directory
const del = require('del');

// browserSync
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;


/// Dev Mode Tasks \\\

// Transform ".scss", minify, rename, add vendor prefixes
gulp.task('styles', function () {
    gulp.src('./app/scss/main.scss')
        .pipe(sass())
        .on('error', function (err) {
            return notify().write(err)
        })
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./app/css'))
        .pipe(reload({stream: true}))
});

// Compress and rename custom JS file
gulp.task('js', () => {
    gulp.src('./app/js/main.js')
        .pipe(uglify())
        .on('error', function (err) {
            return notify().write(err)
        })
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./app/js/'))
        .pipe(reload({stream: true}))
});

// Rewrite "views/partials" inner images paths
gulp.task('rewriteImagePath', function () {
    gulp.src("./app/views/partials/**/*")
    .pipe(rewriteImagePath({path:"../img"}))
});

// Transform ".ejs" files to static ".html" files
gulp.task('html', ['rewriteImagePath'], function () {
    gulp.src('./app/views/*.ejs')
        .pipe(ejs({}, {}, {
            ext: '.html'
        }))
        .on('error', function (err) {
            return notify().write(err)
        })
        .pipe(gulp.dest('./app/html/'))
        .pipe(reload({stream: true}))
});

// browserSync init
gulp.task('browserSync', function () {
    browserSync.init(null, {
        server: {baseDir: "./app"}
    });
});

// Watching files
gulp.task('watch', ['browserSync'], function () {
    gulp.watch(["./app/js/main.js"], ['js'], reload);
    gulp.watch(["./app/scss/**/*"], ['styles'], reload);
    gulp.watch(["./app/views/**/*"], ['html'], reload);
});

gulp.task('default', ['watch', 'html', 'styles', 'js']);


/// Build Mode Tasks \\\

// Always delete "dist" directory before build
gulp.task('clean', function () {
    return del.sync('./dist');
});

// Build Project
gulp.task('build', ['clean'], function () {
    const buildHtml = gulp.src('./app/html/*')
        .pipe(gulp.dest('./dist/html'));
    const buildCss = gulp.src('./app/css/**/*')
        .pipe(gulp.dest('./dist/css'));
    const buildJs = gulp.src(['./app/js/**/*', '!./app/js/main.js'])
        .pipe(gulp.dest('./dist/js'));
    const buildImages = gulp.src('./app/img/*')
        .pipe(gulp.dest('./dist/img'));
    const buildFonts = gulp.src('./app/fonts/**/*')
        .pipe(gulp.dest('./dist/fonts'));
});

